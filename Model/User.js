var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017/test',
	crypto = require('crypto');;

function findOne(username, callback){
	MongoClient.connect(url, function(err, db) {
		var users = db.collection('monitorUsers');
		users.findOne({email:username}, function(err, docs){ 
			callback(null, docs);
			db.close();
		});	
	});
}

function checkDb(){
	MongoClient.connect(url, function(err, db) {
		var md5 = crypto.createHash('md5');
		var password = md5.update('123456').digest('base64');
		var users = db.collection('monitorUsers');
		users.findOne({email:'testUser1'}, function(err, docs){ 
			if(err || !docs || docs =={}) users.insert({email:'testUser1', password:password}, function(){
				db.close();
			})
			else db.close();
		});	
	});
}

var User = {
	findOne: findOne,
	checkDb: checkDb,
}

module.exports = User;