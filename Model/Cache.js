var Cache = function(size){
	this.size = size;
	this._cache = [];
}

Cache.prototype.push = function(item){
	if(this._cache.length >= this.size){
		this._cache.shift();
		this._cache.push(item);
	}
	else this._cache.push(item);
}

Cache.prototype.getAll = function(){return this._cache;}

module.exports = Cache;
