(function(){	
	angular.module('myapp', ['nvd3'])
	.service('userModel', function($http){
		var authenticate = function(callback){
			$http.get('http://localhost:3000/authenticate').then(function(response){
				console.log(response.data);
				callback(response.data.email, response.data.logged);				
			});
		};
		
		var login = function(data, callback){
			$http.post('http://localhost:3000/login',data)
			.then(function(res){
				if(res.data.title=='success'){console.log('log in success');callback(true);}
				else{console.log('log in fail'); callback(false);}
			});
		}
		
		var logout = function(callback){
			$http.delete('http://localhost:3000/logout').then(function(res){
				callback(true);
			})
		}
		
		return {
			getAuth:authenticate,
			login: login,
			logout: logout,
		}
	})
	.controller('data-display', function($scope, $http){
		$scope.options = {
            chart: {
                type: 'stackedAreaChart',
                height: 450,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 30,
                    left: 40
                },
                x: function(d){ /* console.log(d[0]);  */return d[0];},
                y: function(d){return d[1];},
                useVoronoi: false,
                clipEdge: true,
                duration: 100,
                useInteractiveGuideline: true,
                xAxis: {
                    showMaxMin: false,
                    tickFormat: function(d) {
						//console.log('in xAxis it becomes: ' + d);
                        return d3.time.format('%x')((new Date(d)))
                    }
                },
                yAxis: {
                    tickFormat: function(d){
                        return d3.format(',.2f')(d)+'%';
                    }
                },
                zoom: {
                    enabled: true,
                    scaleExtent: [1, 10],
                    useFixedDomain: false,
                    useNiceScale: false,
                    horizontalOff: false,
                    verticalOff: true,
                    unzoomEventType: 'dblclick.zoom'
                }
            }
        };
		$scope.data = [
			{
                "key" : "User Usage" ,
                "values" :  [ ]
            },
		];
		$scope.raw=[['no','data']];
		$http.get('http://localhost:3000/data').then(function(response){
			$scope.data[0].values = response.data;
			$scope.raw = response.data;
		});		
	})
	.controller('loggingCtrl', function($rootScope, $scope, userModel){
		userModel.getAuth(function(username, logged){
			$rootScope.username = username;
			$rootScope.logged = logged;
		})
		$scope.logout = function(){
			userModel.logout(function(res){
				$rootScope.username ='new user';
				$rootScope.logged = false;
			})
		}
	})
	.controller('formCtrl', function($rootScope, $scope, userModel){
		$scope.logging = true;
		$scope.loginFailed = false;
		$scope.login = function(){
			$scope.logging = false;	
			$scope.loginFailed = false;			
			userModel.login({username:$scope.email, password:$scope.password}, function(res){
				if(res){
					$rootScope.username = $scope.email;
					$rootScope.logged = true;
					$('#signInModal').modal('hide');
					$scope.email='', $scope.password='';
				}
				else{
					$scope.loginFailed = true;
				}
			});
		};
		
	});
})();