# README #

This is project implemented with combination of AngularJS, NodeJS, MongoDB, along with modules of Angular-nvd3, Mqtt, passportJS, etc.

### What is this repository for? ###

* This is single page application with the purpose to display data. In this Demo, the data is a fictional parameter generated by the server every second. The parameter ranks between 0 to 100 as to represent persentages. The parameter is published and subscribed by server through the broker Mosquitto.org.
A cache is implemented in the server side to capture the data from last 50 seconds.
* version: 0.0


### How do I get set up? ###

* prerequisities: nodeJS, MongoDB.
* How to start: Clone the repo and run the following commands `npm install` and `bower install`.
* Database configuration: Make sure MongoDB is up and run before run this app. This app with connect with Local MongoDB with no username and password in default.
* How to run: `npm start`

### Who do I talk to? ###

* email: hezhoufan@gmail.com