var express = require('express');
var router = express.Router();
var passport = require('passport'),
	localStrategy = require('passport-local'),
	User = require('../Model/User'),
	mqttClient = require('mqtt').connect('mqtt://test.mosquitto.org'),
	Cache = require('../Model/Cache'); 
	crypto = require('crypto');
	
User.checkDb();
	
//retrieving dummy date -- this is a test, 
//the retrieving will be moved to front-end
//back to the server
var cache = new Cache(50);
mqttClient.subscribe('dummy-num');
mqttClient.on('message', function(topic, message){
	var msgArr = message.toString().split('#');
	var entry = [parseInt(msgArr[0]),parseInt(msgArr[1])];
	cache.push(entry);
});

//configuring passport 
passport.serializeUser(function(user, done) {
  done(null, user.email);
});

passport.deserializeUser(function(email, done) {
  User.findOne(email, function(err, user) {
    done(err, user);
  });
});

//set up new login strategy
passport.use('login', new localStrategy(
	function(username, password, done){
		var md5 = crypto.createHash('md5');
		var pw = md5.update(password).digest('base64');
		User.findOne(username, function (err, user) {		    
		  if (err) { return done(err); }
		  if (!user) {
			return done(null, false, { message: 'Incorrect username.' });
		  }	
			console.log(user.password == pw);
		  if (!(user.password == pw)) {
			return done(null, false, { message: 'Incorrect password.' });
		  }
		  return done(null, user);
		});
	}
));


router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/data', function(req, res, next) {
  res.json(cache.getAll());
});

router.get('/success', function(req, res, next) {
  res.send({ title: 'success' });
});

router.get('/failure', function(req, res, next) {
  res.send({ title: 'failure' });
});

router.get('/api/users/me',
	function(req, res) {
		res.json({ email: req.user });
});

router.get('/authenticate',
	function(req, res) {
		res.json({email: req.user?req.user.email:'new user', logged: req.user?true:false });
});

router.post('/login', passport.authenticate('login', 
	{	successRedirect: '/success',
		failureRedirect: '/failure',
	}
));

router.delete('/logout', function(req, res){
	req.logout();
	res.send({ok:1});
});

module.exports = router;
